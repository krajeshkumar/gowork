export interface StatModel {
    total?: number;
    discharged?: number;
    active?: number;
    deaths?: number;
    date?: Date;
    regional?: any;
    summary?: any;
}
