import { NgModule } from '@angular/core';
import { YesNoComponent } from './yes-no/yes-no.component';
import { PipesModule } from '../pipes/pipes.module';

@NgModule({
    declarations: [
        YesNoComponent
    ],
    imports: [
        PipesModule
    ],
    exports: [
        YesNoComponent
    ],
    providers: [
    ]
})
export class ComponentsModule { }
