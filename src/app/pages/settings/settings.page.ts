import { Component, OnInit } from '@angular/core';
import { AppVersion } from '@ionic-native/app-version/ngx';

@Component({
    selector: 'app-settings',
    templateUrl: './settings.page.html',
    styleUrls: ['./settings.page.scss'],
})
export class SettingsPage implements OnInit {
    versionNumber: string;

    constructor(
        private appVersion: AppVersion,

    ) {
        this.appVersion.getVersionNumber().then(version => {
            this.versionNumber = version;
        });
    }

    ngOnInit() {
    }

}
