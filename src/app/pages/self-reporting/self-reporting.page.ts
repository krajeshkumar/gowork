import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { SelfReportingModel } from 'src/app/models/self-reporting.model';
import { ApiService } from 'src/app/services/api/api.service';
import { CommonUtilsProviderService } from 'src/app/utils/common-utils-provider.service';
import { DatePicker } from '@ionic-native/date-picker/ngx';

@Component({
    selector: 'app-self-reporting',
    templateUrl: './self-reporting.page.html',
    styleUrls: ['./self-reporting.page.scss'],
})
export class SelfReportingPage implements OnInit {
    public selfReportingForm: FormGroup;
    fullName: string;
    team: string;
    url = 'SelfReporting';
    hasRecentTravelHistory: string;
    hasTraveledWithUnknownPeople: string;
    @ViewChild('selfReportingFrm') selfReportingFrm;

    constructor(
        private apiService: ApiService,
        private datePicker: DatePicker,
        private commonUtilsService: CommonUtilsProviderService
    ) {
        this.fullName = 'Rajesh Kumar';
        this.team = 'Iddink';
        this.initMobileForm();
    }

    ngOnInit() {
    }

    initMobileForm(): void {
        this.selfReportingForm = new FormGroup({
            comingFrom: new FormControl('', [Validators.required]),
            modeOfTransport: new FormControl(null, [Validators.required]),
            temperature: new FormControl('', [Validators.required, Validators.min(35), Validators.max(40)]),
            hasFever: new FormControl(null, [Validators.required]),
            hasCough: new FormControl(null, [Validators.required]),
            hasChestPain: new FormControl(null, [Validators.required]),
            hasMask: new FormControl(null, [Validators.required]),
            hasRecentTravelHistoryInState: new FormControl(null, [Validators.required]),
            travelStartDate: new FormControl(null),
            travelEndDate: new FormControl(null),
            travelModeofTransport: new FormControl(null),
            hasRecentHospitalVisit: new FormControl(null, [Validators.required]),
            hospitalVisitDate: new FormControl(null),
            reasonForHospitalVisit: new FormControl(null),
            hasRecentTravelHistory: new FormControl(null, [Validators.required]),
            hasRecentVisitor: new FormControl(null, [Validators.required]),
            visitorArrivalDate: new FormControl(null),
            visitorCity: new FormControl(null),
            isStayingInContainmentZone: new FormControl(null, [Validators.required])
        });
    }

    onDateSelected(dateField: string) {
        this.datePicker.show({
            date: new Date(),
            mode: 'date',
            androidTheme: this.datePicker.ANDROID_THEMES.THEME_DEVICE_DEFAULT_DARK
        }).then(
            date => {
                this.selfReportingForm.controls[dateField].patchValue(date);
            },
            err => console.log('Error occurred while getting date: ', err)
        );
    }

    onAnswerSelected($event, formField) {
        console.log('event = ' + $event);
        this.selfReportingForm.controls[formField].patchValue($event);
    }

    submit() {
        this.selfReportingFrm.submitted = true;
        if (this.selfReportingForm.invalid) {
            this.commonUtilsService.presentToast('Please fill all the required fields');
            return;
        }
        const postData: SelfReportingModel = {
            comingFrom: this.selfReportingForm.value.comingFrom,
            modeOfTransport: this.selfReportingForm.value.modeOfTransport,
            isStayingInContainmentZone: this.selfReportingForm.value.isStayingInContainmentZone === 'y',
            hasRecentTravelHistoryInState: this.selfReportingForm.value.hasRecentTravelHistoryInState === 'y',
            travelModeofTransport: this.selfReportingForm.value.travelModeofTransport,
            travelStartDate: this.selfReportingForm.value.travelStartDate,
            travelEndDate: this.selfReportingForm.value.travelEndDate,
            hasTravelledWithUnknownPeople: this.selfReportingForm.value.hasTraveledWithUnknownPeople === 'y',
            hasRecentHospitalVisit: this.selfReportingForm.value.hasRecentHospitalVisit === 'y',
            hospitalVisitDate: this.selfReportingForm.value.hospitalVisitDate,
            reasonForHospitalVisit: this.selfReportingForm.value.reasonForHospitalVisit,
            hasRecentVisitor: this.selfReportingForm.value.hasRecentVisitor === 'y',
            visitorArrivalDate: this.selfReportingForm.value.visitorArrivalDate,
            visitorCity: this.selfReportingForm.value.visitorCity,
            hasRecentTravelHistory: this.selfReportingForm.value.hasRecentTravelHistory === 'y',
            hasChestPain: this.selfReportingForm.value.hasChestPain === 'y',
            hasCough: this.selfReportingForm.value.hasCough === 'y',
            hasFever: this.selfReportingForm.value.hasFever === 'y',
            hasMask: this.selfReportingForm.value.hasMask === 'y',
            temperature: parseFloat(this.selfReportingForm.value.temperature)
        };

        this.apiService.get(this.url).subscribe(res => {
            console.log(res);
            this.commonUtilsService.presentToast('Saved successfully');
        });

        // this.apiService.post(this.url, postData).subscribe(res => {
        //     console.log(res);
        //     this.commonUtilsService.presentToast('Saved successfully');
        // });
    }

}
