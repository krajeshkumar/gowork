import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AllAppsPage } from './all-apps.page';

const routes: Routes = [
  {
    path: '',
    component: AllAppsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AllAppsPageRoutingModule {}
