import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CovidNewsPageRoutingModule } from './covid-news-routing.module';

import { CovidNewsPage } from './covid-news.page';
import { PipesModule } from 'src/app/pipes/pipes.module';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        CovidNewsPageRoutingModule,
        PipesModule
    ],
    declarations: [CovidNewsPage]
})
export class CovidNewsPageModule { }
