export const environment = {
    production: true
};

export const BaseURL = 'http://gowork-test.us-west-2.elasticbeanstalk.com/api/';
