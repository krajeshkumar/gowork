import { Pipe, PipeTransform } from '@angular/core';
import * as Moment from 'moment';

@Pipe({
    name: 'datePipe',
    pure: false
})
export class DateFormatPipe implements PipeTransform {
    constructor() {
    }

    transform(value: string, format: string) {
        format = format;
        return Moment(value).format(format);
    }
}
