import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ErrorhandlerService } from '../error-handler/errorhandler.service';
import { retry, catchError } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

@Injectable({
    providedIn: 'root'
})
export class NewsService {

    covidNewsApi: string;
    private httpOptions;

    constructor(private http: HttpClient, public errorhandlerService: ErrorhandlerService) {
        this.covidNewsApi = 'https://newsapi.org/v2/top-headlines?country=in&language=en&q=COVID&pageSize=100&page=1';
        this.httpOptions = {
            headers: new HttpHeaders({
                'X-Api-Key': '55a748bfbf434ef8a9027147c5040d14'
            })
        };
    }

    getLatestNews() {
        return this.http.get(this.covidNewsApi, this.httpOptions).pipe(
            retry(1),
            catchError(this.errorhandlerService.handleError)
        );
    }

    // getLatestNews() {
    //     return this.http.get(environment.baseUrl + '/news/in/').pipe(
    //         retry(1),
    //         catchError(this.errorhandlerService.handleError)
    //     );
    // }
}
