import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SelfReportingPageRoutingModule } from './self-reporting-routing.module';

import { SelfReportingPage } from './self-reporting.page';
import { ComponentsModule } from 'src/app/components/components.module';
import { PipesModule } from 'src/app/pipes/pipes.module';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        IonicModule,
        SelfReportingPageRoutingModule,
        ComponentsModule,
        PipesModule
    ],
    declarations: [SelfReportingPage]
})
export class SelfReportingPageModule { }
