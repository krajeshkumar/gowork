import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AllAppsPageRoutingModule } from './all-apps-routing.module';

import { AllAppsPage } from './all-apps.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AllAppsPageRoutingModule
  ],
  declarations: [AllAppsPage]
})
export class AllAppsPageModule {}
