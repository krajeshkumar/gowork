import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
    selector: 'app-yes-no',
    templateUrl: './yes-no.component.html',
    styleUrls: ['./yes-no.component.scss'],
})
export class YesNoComponent implements OnInit {

    @Input() selectedValue: string;
    @Output() selctionEvent = new EventEmitter<string>();

    constructor() { }

    ngOnInit() { }

    getSelectedClass(value: string) {
        return this.selectedValue === value ? 'selected' : '';
    }

    onSelection(value: string) {
        this.selectedValue = value;
        this.selctionEvent.emit(value);
    }
}
