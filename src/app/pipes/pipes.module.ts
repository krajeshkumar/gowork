import { NgModule } from '@angular/core';
import { SortByPipe } from './sort-by';
import { DateFormatPipe } from './date';

@NgModule({
    declarations: [
        SortByPipe,
        DateFormatPipe
    ],
    imports: [],
    exports: [
        SortByPipe,
        DateFormatPipe
    ]
})
export class PipesModule { }
