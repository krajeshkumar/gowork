import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SelfReportingPage } from './self-reporting.page';

const routes: Routes = [
  {
    path: '',
    component: SelfReportingPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SelfReportingPageRoutingModule {}
