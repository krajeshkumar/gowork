import { DecimalPipe } from '@angular/common';

export interface SelfReportingModel {
    comingFrom?: string;
    modeOfTransport?: string;
    isStayingInContainmentZone?: boolean;
    hasRecentTravelHistoryInState?: boolean;
    travelModeofTransport?: string;
    travelStartDate?: string;
    travelEndDate?: string;
    hasTravelledWithUnknownPeople?: boolean;
    hasRecentHospitalVisit?: boolean;
    hospitalVisitDate?: string;
    reasonForHospitalVisit?: string;
    hasRecentTravelHistory?: boolean;
    hasRecentVisitor?: boolean;
    visitorArrivalDate?: string;
    visitorCity?: string;
    temperature?: number;
    hasFever?: boolean;
    hasCough?: boolean;
    hasChestPain?: boolean;
    hasMask?: boolean;
}
