import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NavController } from '@ionic/angular';

@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard.page.html',
    styleUrls: ['./dashboard.page.scss'],
})
export class DashboardPage implements OnInit {

    constructor(
        private router: Router,
        private navCtrl: NavController
    ) { }

    ngOnInit() {
    }

    navigate(navigateToUrl: string) {
        this.navCtrl.navigateForward([navigateToUrl]);
    }
}
