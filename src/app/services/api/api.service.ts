import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { JwtService } from './jwt.service';
import { BaseURL } from 'src/environments/environment';

@Injectable()
export class ApiService {
    constructor(
        private http: HttpClient,
        private jwtService: JwtService
    ) { }

    private setHeaders(): any {
        const headersConfig = {
            'Content-Type': 'application/json'
        };
        // if (this.jwtService.getToken()) {
        //     const token = this.jwtService
        //         .getToken()
        //         .replace('"', '')
        //         .replace('"', '');
        //     headersConfig['Authorization'] = token;
        // }
        return headersConfig;
    }

    get(url: string): Observable<any> {
        return this.http
            .get(`${BaseURL}${url}`, { headers: this.setHeaders() })
            .pipe(catchError(this.handleError));
    }

    put(url: string, body: object = {}): Observable<any> {
        return this.http
            .put(url, body, { headers: this.setHeaders() })
            .pipe(catchError(this.handleError));
    }

    authPost(path: string, body: object = {}): Observable<any> {
        return this.http
            .post(path, body)
            .pipe(catchError(this.handleError));
    }

    post(url: string, body: object = {}): Observable<any> {
        return this.http
            .post(`${BaseURL}${url}`, body, { headers: this.setHeaders() })
            .pipe(catchError(this.handleError));
    }

    delete(url: string, body: object = {}): Observable<any> {
        const token = this.jwtService.getToken().replace('"', '').replace('"', '');
        const options = {
            headers: new HttpHeaders({
                Authorization: token
            }),
            body
        };
        return this.http
            .delete(url, options)
            .pipe(catchError(this.handleError));
    }

    private handleError(error: HttpErrorResponse) {
        if (error.error instanceof ErrorEvent) {
            // A client-side or network error occurred. Handle it accordingly.
            console.error('An error occurred:', error.error.message);
        } else {
            // The backend returned an unsuccessful response code.
            // The response body may contain clues as to what went wrong,
            console.log(
                `Backend returned code ${error.status}, ` + `body was: ${error.message}`
            );
        }
        // return an observable with a user-facing error message
        return throwError(error);
    }
}
