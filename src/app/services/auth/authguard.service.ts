import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Router, CanActivate, RouterStateSnapshot } from '@angular/router';
import { take, map, tap } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class AuthgaurdService implements CanActivate {
    isLoggedIn = false;
    constructor(public router: Router) { }

    setLoggedIn(value: boolean) {
        this.isLoggedIn = value;
    }

    isAuthenticated(): boolean {
        return this.isLoggedIn;
    }

    // canActivate(
    //     next: ActivatedRouteSnapshot,
    //     state: RouterStateSnapshot): Observable<boolean | UrlTree> {
    //     return this.authService.user$.pipe(
    //         take(1),
    //         map(user => !!user),
    //         tap(loggedIn => {
    //             if (!loggedIn) {
    //                 this.router.navigate(['/login']);
    //             }
    //         })
    //     )
    // }

    canActivate(route: ActivatedRouteSnapshot): boolean {
        if (!this.isAuthenticated()) {
            this.router.navigate(['/login']);
            return false;
        }
        return true;
    }
}
