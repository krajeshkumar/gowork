import { Component, OnInit } from '@angular/core';
import { NewsService } from 'src/app/services/news/news.service';
import { CommonUtilsProviderService } from 'src/app/utils/common-utils-provider.service';
import { NewsModel } from 'src/app/models/news.model';

@Component({
    selector: 'app-covid-news',
    templateUrl: './covid-news.page.html',
    styleUrls: ['./covid-news.page.scss'],
})
export class CovidNewsPage implements OnInit {
    latestNews: NewsModel[] = [];

    constructor(
        private newsService: NewsService,
        private utilService: CommonUtilsProviderService
    ) { }

    ngOnInit() {
        this.loadLatestCovidNews();
    }

    async loadLatestCovidNews() {
        await this.utilService.presentLoading('Loading...');
        this.newsService.getLatestNews().subscribe((res: any) => {
            this.utilService.loading.dismiss();
            this.latestNews = res.articles;
        }, error => {
            this.utilService.loading.dismiss();
            this.utilService.presentToast('Error loading News. Please try later');
        });
    }

    getFormattedTitle(news: NewsModel) {
        const lastIndex = news.title.lastIndexOf('-');
        return news.title.substring(0, lastIndex);
    }

    openFullNews(news: NewsModel) {
        // const options: InAppBrowserOptions = {
        //     hidenavigationbuttons: 'yes',
        //     hideurlbar: 'yes',
        //     zoom: 'no',
        //     toolbarcolor: '#123d6a',
        //     closebuttoncolor: '#ffffff'
        // };
        // this.iab.create(news.url, '_blank', options);
        // window.open(news.url);
    }
}
