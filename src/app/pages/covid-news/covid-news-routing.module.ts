import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CovidNewsPage } from './covid-news.page';

const routes: Routes = [
  {
    path: '',
    component: CovidNewsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CovidNewsPageRoutingModule {}
