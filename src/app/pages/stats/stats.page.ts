import { Component, OnInit, ViewChild } from '@angular/core';
import { StatModel } from 'src/app/models/stat.model';
import { CovidApiService } from 'src/app/services/covid-api/covid-api.service';
import { IonContent } from '@ionic/angular';
import { CommonUtilsProviderService } from 'src/app/utils/common-utils-provider.service';
import * as Moment from 'moment';
import _ from 'lodash';
import * as Chart from 'chart.js';

@Component({
    selector: 'app-stats',
    templateUrl: './stats.page.html',
    styleUrls: ['./stats.page.scss'],
})
export class StatsPage implements OnInit {
    updateTime: string;
    covidDataLatest: any;
    // loading: HTMLIonLoadingElement;
    showInfo = false;
    casesOverview: any[];
    activePercentage = 0;
    recoveredPercentage = 0;
    deceasedPercentage = 0;
    yesterdayData: StatModel;
    selectedState: any;
    summary: any;
    contactNumber: string;
    allContacts: any;
    @ViewChild('ionContent') ionContent: IonContent;
    @ViewChild('caseOverviewPieCanvas') caseOverviewPieCanvas;

    constructor(
        private covidApiService: CovidApiService,
        private utilService: CommonUtilsProviderService
    ) {
        this.yesterdayData = {
            total: 0,
            active: 0,
            deaths: 0,
            discharged: 0
        };
    }

    async ngOnInit() {
        await this.utilService.presentLoading('Loading...');
        this.covidApiService.getLatestData().subscribe((latest: any) => {
            this.covidDataLatest = latest.data;
            this.summary = this.covidDataLatest.summary;
            this.updateTime = latest.lastOriginUpdate;
            this.acrossIndiaStats();

            this.covidApiService.getHistoryData().subscribe((history: any) => {
                this.utilService.loading.dismiss();
                const today = new Date();
                const yesterday = Moment().subtract(1, 'days').format('YYYY-MM-DD');
                const yesterdayData = _.find(history.data, d => d.day === yesterday);
                this.yesterdayData = {
                    total: yesterdayData.summary.total,
                    deaths: yesterdayData.summary.deaths,
                    discharged: yesterdayData.summary.discharged,
                    regional: yesterdayData.regional,
                    summary: yesterdayData.summary
                };

                this.covidApiService.getContactDetails().subscribe((contactDetails: any) => {
                    this.allContacts = contactDetails.data.contacts;
                    this.contactNumber = this.allContacts.primary['number-tollfree'];
                });
            }, error => {
                this.utilService.loading.dismiss();
                this.utilService.presentToast('Error loading COVID-19 history data');
            });
        }, error => {
            this.utilService.loading.dismiss();
            this.utilService.presentToast('Error loading COVID-19 latest data');
        });
    }

    private acrossIndiaStats() {
        const totalCases = this.summary.total;
        this.recoveredPercentage = Math.round((this.summary.discharged / totalCases) * 100);
        this.deceasedPercentage = Math.round((this.summary.deaths / totalCases) * 100);
        this.activePercentage = Math.round((this.getActiveCases(this.covidDataLatest) / totalCases) * 100);
        this.casesOverview = [{
            label: 'active',
            color: '#4fa9e4',
            count: this.getActiveCases(this.covidDataLatest)
        }, {
            label: 'recovered',
            color: '#63c331',
            count: this.summary.discharged
        }, {
            label: 'deceased',
            color: '#eb3d85',
            count: this.summary.deaths
        }];
        this.pieChart();
    }

    scrollContent(scroll) {
        if (scroll === 'top') {
            this.ionContent.scrollToTop(300);
        } else {
            this.ionContent.scrollToBottom(300);
        }
    }

    loadStateStats(stateData) {
        if (stateData.loc === this.selectedState) {
            this.resetStat();
            return;
        }
        this.selectedState = stateData.loc;
        this.summary = stateData;
        this.summary.total = stateData.totalConfirmed;
        this.recoveredPercentage = Math.round((this.summary.discharged / stateData.totalConfirmed) * 100);
        this.deceasedPercentage = Math.round((this.summary.deaths / stateData.totalConfirmed) * 100);
        this.activePercentage = Math.round((
            (stateData.totalConfirmed - (stateData.discharged + stateData.deaths)) / stateData.totalConfirmed) * 100);
        const yesterdayData = _.find(this.yesterdayData.regional, r => r.loc === stateData.loc);
        this.yesterdayData = {
            total: yesterdayData.totalConfirmed,
            deaths: yesterdayData.deaths,
            discharged: yesterdayData.discharged,
            regional: this.yesterdayData.regional,
            summary: this.yesterdayData.summary
        };
        this.casesOverview = [{
            label: 'active',
            color: '#4fa9e4',
            count: stateData.totalConfirmed - (stateData.discharged + stateData.deaths)
        }, {
            label: 'recovered',
            color: '#63c331',
            count: stateData.discharged
        }, {
            label: 'deceased',
            color: '#eb3d85',
            count: stateData.deaths
        }];

        // Get contact details
        const regionContact = _.find(this.allContacts.regional, region => region.loc === stateData.loc);
        if (regionContact != null) {
            this.contactNumber = regionContact.number.split(',')[0];
        }
        this.pieChart();
        this.scrollContent('top');
    }

    getActiveCases(data) {
        return data.summary.total - (data.summary.discharged + data.summary.deaths);
    }

    showInformation() {
        this.showInfo = true;
    }

    onInfoSelected(event) {
        this.showInfo = false;
    }

    resetStat() {
        this.selectedState = null;
        this.summary = this.covidDataLatest.summary;
        this.acrossIndiaStats();
        this.yesterdayData = {
            total: this.yesterdayData.summary.total,
            deaths: this.yesterdayData.summary.deaths,
            discharged: this.yesterdayData.summary.discharged,
            regional: this.yesterdayData.regional,
            summary: this.yesterdayData.summary
        };
        this.contactNumber = this.allContacts.primary['number-tollfree'];
    }

    pieChart() {
        let ctx: CanvasRenderingContext2D;

        if (this.caseOverviewPieCanvas.nativeElement !== undefined) {
            const canvas = this.caseOverviewPieCanvas.nativeElement as HTMLCanvasElement;
            ctx = canvas.getContext('2d');
        } else {
            ctx = this.caseOverviewPieCanvas.ctx;
        }

        const data = _.map(this.casesOverview, 'count');
        const total = _.sumBy(this.casesOverview, 'count');
        const backgroundColor = _.map(this.casesOverview, (category) => {
            return category.color;
        });

        const labels = _.chain(this.casesOverview).map(c => c.label).value();
        this.caseOverviewPieCanvas = new Chart(ctx, {
            type: 'pie',
            data: {
                labels,
                datasets: [
                    {
                        data,
                        backgroundColor,
                        borderWidth: 2
                    }
                ]
            },
            options: {
                cutoutPercentage: 65,
                layout: {
                    // padding: {
                    //     bottom: 35,
                    //     top: -10,
                    //     left: 70,
                    //     right: 70
                    // }
                },
                title: {
                    display: true
                },
                maintainAspectRatio: false,
                // plugins: {
                //     datalabels: {
                //         anchor: 'end',
                //         align: 'end',
                //         font: {
                //             size: 10,
                //         },
                //         color: '#123D6A',
                //         textAlign: 'center',
                //         clamp: true,
                //         display(context) {
                //             const value = context.chart.data.datasets[0].data[context.dataIndex] as number;
                //             return (value / total) > 0.08;
                //         },
                //         formatter(value, context) {
                //             const data = context.chart.data.datasets[0].data[context.dataIndex] as number;
                //             return [context.chart.data.labels[context.dataIndex], Math.round(data * 100 / total) + '%'];
                //         }
                //     }
                // },
                events: ['click'],
                onClick(params: any) {
                    console.log(params);
                },
                legend: {
                    display: false,
                    position: 'right',
                    labels: {
                        fontSize: 10,
                        boxWidth: 15
                    }
                },
                tooltips: {
                    enabled: false,
                }
            }
        });
    }

    // async presentToast(msg: string) {
    //     const toast = await this.toastCtrl.create({
    //         message: msg,
    //         duration: 2000
    //     });
    //     toast.present();
    // }

    // async presentLoading(msg) {
    //     this.loading = await this.loadingCtrl.create({
    //         message: msg,
    //     });
    //     return await this.loading.present();
    // }
}
