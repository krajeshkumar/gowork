import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomePage } from './home.page';

const routes: Routes = [
    {
        path: '',
        component: HomePage,
        children: [
            {
                path: 'dashboard',
                loadChildren: () => import('../dashboard/dashboard.module').then(m => m.DashboardPageModule)
            },
            {
                path: 'settings', loadChildren: () => import('../settings/settings.module')
                    .then(m => m.SettingsPageModule)
            },
            {
                path: 'news', loadChildren: () => import('../covid-news/covid-news.module')
                    .then(m => m.CovidNewsPageModule)
            },
            {
                path: 'stats', loadChildren: () => import('../covid-stats/covid-stats.module')
                    .then(m => m.CovidStatsPageModule)
            },
        ]
    },
    {
        path: '',
        redirectTo: '/home/dashboard',
        pathMatch: 'full'
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class HomePageRoutingModule { }
