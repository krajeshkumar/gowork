import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CovidStatsPage } from './covid-stats.page';

const routes: Routes = [
  {
    path: '',
    component: CovidStatsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CovidStatsPageRoutingModule {}
