import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NavController } from '@ionic/angular';

@Component({
    selector: 'app-all-apps',
    templateUrl: './all-apps.page.html',
    styleUrls: ['./all-apps.page.scss'],
})
export class AllAppsPage implements OnInit {

    constructor(
        private router: Router,
        public navCtrl: NavController
    ) { }

    ngOnInit() {
    }

    goToAtHome() {
        this.navCtrl.navigateRoot('/home/dashboard');
    }
}
