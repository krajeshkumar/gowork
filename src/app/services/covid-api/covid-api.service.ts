import { Injectable } from '@angular/core';

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { ErrorhandlerService } from '../error-handler/errorhandler.service';
import { retry, catchError } from 'rxjs/operators';
import { Observable, throwError } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class CovidApiService {

    covidApi: string;

    constructor(private http: HttpClient, public errorhandlerService: ErrorhandlerService) {
        this.covidApi = 'https://api.rootnet.in/covid19-in/';
    }

    getLatestData() {
        return this.http.get(this.covidApi + 'stats/latest').pipe(
            retry(1),
            catchError(this.errorhandlerService.handleError)
        );
    }

    getHistoryData() {
        return this.http.get(this.covidApi + 'stats/history').pipe(
            retry(1),
            catchError(this.errorhandlerService.handleError)
        );
    }

    getContactDetails() {
        return this.http.get(this.covidApi + '/contacts').pipe(
            retry(1),
            catchError(this.errorhandlerService.handleError)
        );
    }

}
