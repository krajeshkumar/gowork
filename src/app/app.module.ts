import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { CommonUtilsProviderService } from './utils/common-utils-provider.service';
import { ApiService } from './services/api/api.service';
import { JwtService } from './services/api/jwt.service';
import { DatePicker } from '@ionic-native/date-picker/ngx';

@NgModule({
    declarations: [AppComponent],
    entryComponents: [],
    imports: [BrowserModule, IonicModule.forRoot(), HttpClientModule, AppRoutingModule],
    providers: [
        StatusBar,
        SplashScreen,
        CommonUtilsProviderService,
        ApiService,
        JwtService,
        DatePicker,
        { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
