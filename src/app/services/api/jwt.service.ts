import { Injectable } from '@angular/core';

@Injectable()
export class JwtService {

    getToken(): string {
        return window.localStorage.jwtToken;
    }

    saveToken(token: string) {
        window.localStorage.jwtToken = token;
    }
    getUser() {
        return window.localStorage.user;
    }
    saveUser(user: any) {
        window.localStorage.user = user;
    }

    destroyToken() {
        window.localStorage.removeItem('jwtToken');
    }
}
