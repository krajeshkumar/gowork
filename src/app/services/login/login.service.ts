import { Injectable } from '@angular/core';
import { ApiService } from '../api/api.service';
import { BaseURL } from 'src/environments/environment';

@Injectable({
    providedIn: 'root'
})
export class LoginService {

    constructor(public apiService: ApiService) { }

    login(username: string, password: string) {
        const body = { username, password };
        return this.apiService.authPost(BaseURL, body);
    }
}
