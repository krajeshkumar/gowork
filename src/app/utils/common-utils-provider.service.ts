import { Injectable } from '@angular/core';
import { ToastController, AlertController, LoadingController } from '@ionic/angular';

@Injectable({
    providedIn: 'root'
})
export class CommonUtilsProviderService {
    toast: HTMLIonToastElement;
    loading: HTMLIonLoadingElement;

    constructor(
        private loadingCtrl: LoadingController,
        private toastCtrl: ToastController,
        private alertCtrl: AlertController
    ) { }

    async presentToast(msg: string) {
        try {
            this.toast.dismiss();
        } catch (e) { }

        this.toast = await this.toastCtrl.create({
            message: msg,
            position: 'bottom',
            duration: 3000
        });
        this.toast.present();
        return this.toast;
    }

    async presentLoading(msg) {
        this.loading = await this.loadingCtrl.create({
            message: msg
        });
        return await this.loading.present();
    }

}
