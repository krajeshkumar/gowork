export class AppConstants {

    public static packageName = 'com.comakeit.goWork';

    public static noInternet = 'Please check your internet connection and retry.';

}
