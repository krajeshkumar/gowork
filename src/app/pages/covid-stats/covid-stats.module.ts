import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CovidStatsPageRoutingModule } from './covid-stats-routing.module';

import { CovidStatsPage } from './covid-stats.page';
import { CovidApiService } from 'src/app/services/covid-api/covid-api.service';
import { PipesModule } from 'src/app/pipes/pipes.module';
import { SortByPipe } from 'src/app/pipes/sort-by';
import { DateFormatPipe } from 'src/app/pipes/date';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        PipesModule,
        CovidStatsPageRoutingModule
    ],
    declarations: [CovidStatsPage],
    providers: [
        CovidApiService,
    ]
})
export class CovidStatsPageModule { }
